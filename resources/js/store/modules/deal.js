export const state = {
    scannerPackerDeal: null
};

export const actions = {
    async fetchScannerPackerDeal({commit}, id) {
        const response = await axios.get(`/packer/deals/${id}/`);
        commit('updateScannerPackerDeal', response.data);
    }
};

export const mutations = {
    updateScannerPackerDeal(state, deal) {
        state.scannerPackerDeal = deal;
    }
};

export const getters = {
    getScannerPackerDeal(state) {
        return state.scannerPackerDeal;
    }
};
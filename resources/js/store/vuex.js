import Vue from 'vue';
import Vuex from "vuex";
import * as deal from "./modules/deal";

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        defaultEnv: 'hello'
    },
    actions: {},
    mutation: {},
    getters: {},
    modules: {
        deal,
    },
});
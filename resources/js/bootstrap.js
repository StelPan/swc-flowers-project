import * as Bootstrap from 'bootstrap';
import 'bootstrap-icons/font/bootstrap-icons.min.css';

import axios from './plugins/axios';

window.Bootstrap = Bootstrap;

window.axios = axios;
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

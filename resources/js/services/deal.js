/**
 *
 * @param id
 * @returns {Promise<any>}
 */
export const loadDeal = async (id) => {
    const response = await axios.get(`/ajax/deals/${id}`);
    return response.data;
};

export const loadEasyDeal = async (id) => {
    const response = await axios.get(`/ajax/deals/${id}/easy`);
    return response.data;
}

/**
 *
 * @param id
 * @param params
 * @returns {Promise<*>}
 */
export const updateDocketDeal = async (id, params) => {
    const response = await axios.post(`/ajax/deals/${id}/docket`, params);
    return response.data;
}

/**
 *
 * @param id
 * @param params
 * @returns {Promise<any>}
 */
export const updateStorage = async (id, params = {}) => {
    const response = await axios.post(`/ajax/deals/${id}/storage`, params);
    return response.data;
}

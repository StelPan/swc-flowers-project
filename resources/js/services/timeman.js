/**
 *
 * @returns {Promise<*>}
 */
export const open = async () => {
    let response = await axios.get('/ajax/timeman/open')
    return response.data;
};

/**
 *
 * @returns {Promise<*>}
 */
export const pause = async () => {
    let response = await axios.get('/ajax/timeman/pause')
    return response.data;
};

/**
 *
 * @returns {Promise<*>}
 */
export const close = async () => {
    let response = await axios.get('/ajax/timeman/close')
    return response.data;
};

/**
 *
 * @returns {Promise<*>}
 */
export const status = async () => {
    let response = await axios.get('/ajax/timeman/status')
    return response.data;
};

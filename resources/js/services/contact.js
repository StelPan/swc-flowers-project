export const loadContact = async (params) => {
    const response = await axios.get(`/ajax/contacts`, {params});
    return response.data;
};
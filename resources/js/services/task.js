export const loadTask = async (id) => {
    const response = await axios.get(`/ajax/tasks/${id}`);
    return response.data;
};

export const taskAssemblyFlavor = async (id) => {
    const response = await axios.post(`/ajax/tasks/${id}/set-by-qr`);
    return response.data;
};

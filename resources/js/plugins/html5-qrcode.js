import {Html5QrcodeScanner, Html5Qrcode} from "html5-qrcode";

const config = {
    qrbox: 200,
    fps: 15
};

const verbose = false;

function createHtml5Qrcode(selector) {
    return new Html5Qrcode(selector, verbose);
}

export {createHtml5Qrcode, config, Html5Qrcode};

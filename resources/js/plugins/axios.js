import axios from "axios";
import Vue from "vue";

let instance = axios.create({
    baseUrl: process.env.MIX_APP_URL,
});

instance.interceptors.response.use(function (response) {
    return response;
}, function (error) {
    if (error.response) {
        console.log(error.response.data)
        switch (error.response.status) {
            case 404:
                Vue.$toast.open({
                    position: 'top-right',
                    message: error.response.data.message,
                    type: 'error'
                });

                break;
            case 400:
                Vue.$toast.open({
                    position: 'top-right',
                    message: error.response.data.message,
                    type: 'error'
                });

                break;
            case 403:
                Vue.$toast.open({
                    position: 'top-right',
                    message: error.response.data.message,
                    type: 'error'
                });

                break;
            case 422:
                Vue.$toast.open({
                    position: 'top-right',
                    message: error.response.data.message,
                    type: 'error'
                });

                break;
        }
    }
})

export default instance;

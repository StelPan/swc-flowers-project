import Vue from "vue";
import ToastPlugin from "vue-toast-notification";
import 'vue-toast-notification/dist/theme-default.css';

Vue.use(ToastPlugin);

import './bootstrap';
import Vue from 'vue/dist/vue.js';
import {store} from './store/vuex';

// Vue.config.productionTip = false;
// Vue.config.devtools = false;
// Vue.config.debug = false;
// Vue.config.silent = true;

import './plugins/vue-toast-notification';

Vue.component('grower-timer-form', require('./components/form/GrowerTimerForm.vue').default);
Vue.component('grower-timer', require('./components/timers/GrowerTimer.vue').default);
Vue.component('create-lead-form', require('./components/form/CreateLeadForm.vue').default);

Vue.component('task-form', require('./components/form/TaskForm.vue').default);
Vue.component('task-card', require('./components/cards/TaskCard.vue').default);

Vue.component('show-modal-button', require('./components/buttons/ShowModalButton').default);

Vue.component('packer-scanner-deal-modal', require('./components/modals/PackerScannerDealModal').default);
Vue.component('show-deal-modal', require('./components/modals/ShowDealModal').default);

Vue.component('create-lead-component', require('./components/views/CreateLeadComponent.vue').default);
Vue.component('create-task-component', require('./components/views/CreateTaskComponent.vue').default);
Vue.component('logistician-view-component', require('./components/views/LogisticianViewComponent.vue').default);

// For packer

Vue.component('search-deal-view', require('./components/views/packer/SearchDealView.vue').default);
Vue.component('storage-deal-view', require('./components/views/packer/StorageView.vue').default);

const app = new Vue({
    el: '#app',
    store,
});

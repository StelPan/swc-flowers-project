@extends('layouts.app')

@section('title', 'Стикеры и маршрутные листы')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
            </div>
            <div class="col-lg-12 mb-2">
                <div class="d-flex justify-content-center">
                    <span class="text-center">
                        Сканируйте QR сделки для получения информации
                    </span>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="d-flex justify-content-center">
                    <logistician-view-component :camera_index='{{ $camera_index }}'></logistician-view-component>
                </div>
            </div>
        </div>
    </div>
@endsection

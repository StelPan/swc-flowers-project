@extends('layouts.app')

@section('title', 'Сканер места хранения')

@section('content')
    <storage-deal-view
        :deal="{{ json_encode($deal) }}"
        title="Установка троли и полки"
    ></storage-deal-view>
@endsection

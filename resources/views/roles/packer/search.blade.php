@extends('layouts.app')

@section('title', 'Поиск сделки')

@section('content')
    @if($errors->any())
        <div class="container">
            <div class="row col-12">
                <div class="alert alert-danger">
                    {{ $errors->first() }}
                </div>
            </div>
        </div>
    @endif
    <search-deal-view></search-deal-view>

{{--    @include('components.footers.packer-footer')--}}
@endsection

@extends('layouts.app')

@section('title', 'Поиск клиента')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                @if(session()->has('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif

                @if($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                              {{ $error }}
                            @endforeach
                        </ul>
                    </div>
                @endif

                <create-lead-component :action="'{{ route('leads.store') }}'">
                    @csrf
                    @method('POST')
                </create-lead-component>
            </div>
        </div>
    </div>
@endsection

@extends('layouts.app')

@section('title', 'Задача на выполнение')

@section('content')
    <div class="container">
        <div class="row mb-3">
            <div class="col-lg-12">
                <a class="text-decoration-none text-uppercase text-danger" href="{{ route('growers.tasks') }}">
                    < Назад
                </a>
            </div>
            <div class="col-lg-12">
                @if(session()->has('grower.status.edit'))
                    <div class="alert alert-success">
                        {{ session()->get('grower.status.edit') }}
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <task-form :deal="{{ json_encode($deal) }}">
                    @csrf

                    @method('POST')
                </task-form>
            </div>
        </div>
    </div>
@endsection

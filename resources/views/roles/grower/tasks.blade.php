@extends('layouts.app')

@section('title', 'Задачи на флориста')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">

                @if($errors->any())
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </div>
                @endif

                <div class="row mb-2">
                    <div class="col-4"></div>
                    <div class="col-4">
                        <div class="d-flex justify-content-center align-items-center h-100">
                            <h1 class="text-uppercase fw-bold text-purple-light m-0 p-0">Заказы</h1>
                        </div>
                    </div>
                    <div class="col-4 d-flex justify-content-end align-items-center">
                        <create-task-component></create-task-component>
                    </div>
                </div>
            </div>

            <div class="col-lg-12 mb-5">
                <div class="row">
                    <div class="col-lg-12">
                        @if(session()->has('tasks.task.completed'))
                            <div class="alert alert-success">
                                {{ session()->get('tasks.task.completed') }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>


            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        @forelse($tasks as $task)
                            <a
                                class="text-decoration-none text-black"
                                href="{{ route('growers.tasks.edit', ['id' => $task['id']]) }}"
                            >
                                <div class="row mb-2">
                                    @php $carbon = \Carbon\Carbon::make($task['createdDate']); @endphp
                                    <div class="col-lg-12 px-4">
                                        <div class="row border border-1">
                                            <div class="col-lg-12">
                                                <div class="row d-flex">
                                                    <div class="col-10">
                                                        <span style="word-break: break-all">{{ $task['title'] }}</span>
                                                    </div>
                                                    <div class="col-2 p-0">
                                                <span class="mr-2">
                                                    {{
                                                        ($carbon->hour < 10 ? '0'. $carbon->hour : $carbon->hour) . ':'
                                                        . ($carbon->minute < 10 ? '0'. $carbon->minute : $carbon->minute)
                                                    }}
                                                </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="d-flex flex justify-content-start">
                                                    <span class="fw-bold">Статус заказа:&nbsp;</span>
                                                    <span class="fw-bold">{{ \App\Types\Task\StatusMessage::getMessage($task['status']) }}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        @empty
                            <div class="d-flex justify-content-center">
                                <span>Список задач отсутствует</span>
                            </div>
                        @endforelse
                    </div>
                </div>
            </div>
        </div>

        @include('components.footers.grower-menu-footer')
    </div>
@endsection

@extends('layouts.app')

@section('title', 'Чаты')

@section('content')
		<div class="container">
				<div class="row">
						<div class="col-lg-12">
								<h1>Чаты whatsupp</h1>
						</div>

						<div class="col-lg-12">
								<iframe
										class="w-100 vh-100"
										allow="microphone *; clipboard-write *"
										src="{{ $wic }}"
										frameborder="0"></iframe>
						</div>
				</div>
				@include('components.footers.grower-menu-footer')
		</div>
@endsection
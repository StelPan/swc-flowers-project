@extends('layouts.app')

@section('title', 'Просмотр фотографии')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <a class="text-decoration-none text-uppercase text-danger"
                   href="{{ route('growers.tasks.edit', ['id' => $id]) }}">
                    < Назад
                </a>
            </div>
            <div class="col-lg-12">
                <img class="w-100" src="{{ $url }}" alt="">
            </div>
        </div>
    </div>
@endsection

@extends('layouts.app')

@section('title', 'Роль - Флорист | Florcat')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="d-flex justify-content-center mb-5">
                    <span class="text-uppercase fw-bold text-danger fs-4">Рабочий день</span>
                </div>
                <div class="d-flex justify-content-center mb-5">
                    <div class="user-content border border-1 border-secondary px-5 py-2">
                        <span class="text-center fw-medium fs-3">
                            {{ strlen(auth()->user()->name) ? auth()->user()->name : 'Имя не указано' }}
                        </span>
                    </div>
                </div>
            </div>
        </div>

        <grower-timer-form></grower-timer-form>

        @include('components.footers.grower-menu-footer')
    </div>
@endsection

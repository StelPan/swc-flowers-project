@extends('layouts.app')

@section('title', 'Пользователи')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1>Сотрудники Б24</h1>

                @if(session()->has('admin.user.created'))
                    <div class="alert alert-success">
                        {{ session()->get('admin.user.created') }}
                    </div>
                @endif

                <div class="table-responsive">
                    <table class="table">

                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Синхронизация</th>
                            <th scope="col">ФИО</th>
                            <th scope="col">Телефон</th>
                            <th scope="col">Почта</th>
                            <th scope="col">Роли</th>
                            <th scope="col">Действия</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($bitrixUsers as $bitrixUser)
                            @php
                                $localUser = $users->first(function ($user) use ($bitrixUser) {
                                    return $user->bitrix_id === $bitrixUser['ID'];
                                });
                            @endphp

                            <tr>
                                <th scope="row">{{ $bitrixUser['ID'] }}</th>
                                <td>{{ $localUser ? 'Синхронизован' : 'Не синхронизован' }}</td>
                                <td>{{ $bitrixUser['NAME'] ?? 'Не задано' }}</td>
                                <td>{{ $bitrixUser['PHONE'] ?? 'Не задано' }}</td>
                                <td>{{ $bitrixUser['EMAIL'] ?? 'Email не указан'  }}</td>
                                <td>
                                    @if($localUser)
                                        @foreach($localUser->roles as $role)
                                            {{ $role->ru_name }},
                                        @endforeach
                                    @endif
                                </td>
                                <td>
                                    @if($localUser)
                                        <a class="btn btn-primary"
                                           href="{{ route('users.edit', ['user' => $localUser]) }}">Обновить права</a>
                                    @else
                                        <a class="btn btn-success"
                                           href="{{ route('users.create', ['user' => $bitrixUser['ID']]) }}">Создать
                                            пользователя</a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@extends('layouts.app')

@section('title', 'Изменение прав пользователя')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                @if(session()->get('success'))
                    <div class="alert alert-success">
                        Права успешно изменены.
                        @if(session()->has('password'))
                            Новый пароль: {{ session()->get('password') }}
                        @endif
                    </div>
                @endif

                <h1>Изменение прав пользователя</h1>

                <form action="{{ route('users.update', ['user' => $user]) }}" method="POST">'
                    @csrf
                    @method('PUT')
                    <div class="form-group mb-2">
                        <label for="email">Адрес электронной почты</label>
                        <input type="email" id="email" class="form-control" value="{{ $user->email }}" disabled>
                    </div>

                    <div class="form-group mb-2">
                        <label for="roles">Роли</label>
                        <select name="roles[]" id="roles" class="form-control" multiple size="5">
                            @foreach($roles as $role)
                                @php
                                    $exist = $user->roles->first(fn ($r) => $r->id === $role->id);
                                @endphp

                                <option value="{{ $role->id }}" @if($exist) selected @endif>
                                    {{ $role->ru_name }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group mb-2">
                        <label for="generate">Сгенерировать новый пароль</label>
                        <input type="checkbox" value="1" id="generate" name="generate">
                    </div>

                    <div class="form-group">
                        <button class="btn btn-success">
                            Обновить
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

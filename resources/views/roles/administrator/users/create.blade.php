@extends('layouts.app')

@section('title', 'Создание пользователя')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1>Создание пользователя и указание прав</h1>

                <form action="{{ route('users.store', ['user' => $externalUser['ID']]) }}" method="post">
                    @csrf

                    @method('POST')

                    <div class="form-group mb-2">
                        <label for="roles">Роли сотрудника</label>
                        <select name="roles[]" multiple id="roles" class="form-control">
                            <option selected disabled>Выберите роли</option>
                            @foreach($roles as $role)
                                <option value="{{ $role->id }}">{{ $role->ru_name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <button class="btn btn-success">
                            Создать
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@extends('layouts.default')

@section('title', 'Авторизация')

@section('content')
    <div class="d-flex justify-content-center align-items-center position-absolute w-100 h-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3">
                    <div class="d-flex justify-content-center mb-5 px-2">
                        <img src="{{ asset('/img/logo.png') }}" class="w-50" alt="">
                    </div>

                    <div class="error-alert mb-3">
                        <div class="d-flex justify-content-center text-danger">
                            @error('email')
                            {{ trans('auth.failed', [], 'ru') }}
                            @enderror

                            @error('password')
                            {{ trans('auth.failed', [], 'ru') }}
                            @enderror
                        </div>
                    </div>

                    <div class="login-form">
                        <form method="POST" action="{{ route('login') }}">
                            @csrf

                            <div class="mb-3">
                                <input id="email" type="email"
                                       class="rounded-0 form-control"
                                       name="email" value="{{ old('email') }}" required autocomplete="email" autofocus
                                       placeholder="введите логин">
                            </div>

                            <div class="mb-3">
                                <input id="password" type="password"
                                       class="rounded-0 form-control"
                                       name="password" required
                                       autocomplete="current-password" placeholder="введите пароль">
                            </div>

                            <div class="mb-3 d-flex justify-content-center">
                                <button type="submit" class="rounded-0 btn btn-purple-light w-100 mx-4">
                                    {{ __('ВОЙТИ') }}
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

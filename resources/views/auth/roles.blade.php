@extends('layouts.app')

@section('title', 'Установка роли сотрудника')

@section('content')
		<div class="container">
				<div class="row">
						<div class="col-lg-12">
								<h1>Установка роли пользователя</h1>
						</div>
						<div class="col-lg-12">
								<form action="{{ route('auth.roles.set') }}" method="post">
										@method('post')
										@csrf

										<div class="form-group mb-2">
												<label for="roles">Роли</label>
												<select name="roles[]" id="roles" class="form-control" multiple required>
														<option selected disabled>Выберите роли</option>

														@foreach($roles as $role)
																<option value="{{ $role->id }}">{{ $role->ru_name }}</option>
														@endforeach
												</select>
										</div>

										<button type="submit" class="btn btn-purple-light">Установить роли</button>
								</form>
						</div>
				</div>
		</div>
@endsection
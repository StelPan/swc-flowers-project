<div class="modal fade" id="qrScannerContentModal" tabindex="-1" aria-labelledby="qrScannerContentModal"
     aria-hidden="true">
		<div class="modal-dialog h-100">
				<div class="modal-content h-50">
						<div class="modal-header">
								<h1 class="modal-title fs-5" id="exampleModalLabel">Сделка</h1>
								<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
						</div>
						<div class="modal-body h-50" id="modal-body">
								<form action="">
										@csrf('')

										@method('POST')
								</form>
						</div>
				</div>
		</div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog h-100">
        <div class="modal-content h-50">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="exampleModalLabel">Сканируйте QR</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body h-50" id="qr-scanner">

            </div>
        </div>
    </div>
</div>
<div class="fixed-bottom bg-white" style="height: 50px !important;">
    <hr class="m-0 p-0">
    <div class="row h-100">
        <div class="col-6 d-flex justify-content-center align-items-center">
            <div class="d-flex justify-content-center">
                <a
                    href="{{ route('growers.index') }}"
                    class="text-center text-decoration-none text-purple-light
                    @if(url()->current() === route('growers.index')) fw-bold  @endif"
                >
                    <span class="text-uppercase">Рабочий день</span>
                </a>
            </div>
        </div>
        <div class="col-6 d-flex justify-content-center align-items-center">
            <div class="d-flex justify-content-center">
                <a
                    href="{{ route('growers.tasks') }}"
                    class="text-center text-decoration-none text-purple-light
                    @if(url()->current() === route('growers.tasks')) fw-bold  @endif"
                >
                    <span class="text-uppercase">Заказы</span>
                </a>
            </div>
        </div>
    </div>
</div>

<div class="fixed-bottom bg-white">
    <hr class="m-0 p-0">
    <div class="row">
        <div class="col-12 d-flex justify-content-center align-items-center">
            <div class="d-flex justify-content-center">
                <a
                    href="{{ route('packer.search-deal') }}"
                    class="text-center text-decoration-none text-purple-light
                    @if(url()->current() === route('growers.index')) fw-bold  @endif"
                >
                    <span class="text-uppercase">Место хранения</span>
                </a>
            </div>
        </div>
{{--        <div class="col-6 d-flex justify-content-center align-items-center">--}}
{{--            <div class="d-flex justify-content-center">--}}
{{--                <a--}}
{{--                    href="{{ route('growers.tasks') }}"--}}
{{--                    class="text-center text-decoration-none text-purple-light--}}
{{--                    @if(url()->current() === route('growers.tasks')) fw-bold  @endif"--}}
{{--                >--}}
{{--                    <span class="text-uppercase">Заказы</span>--}}
{{--                </a>--}}
{{--            </div>--}}
{{--        </div>--}}
    </div>
</div>

<?php

namespace App\ModelsSynchronization;

class ContactSynchronizationModel extends AbstractSynchronizationModel
{
    protected array $fieldsAssociation = [
        "ID" => "bitrix_contact_id",
        "PHONE" => 'phone',
        "POST" => null,
        "COMMENTS" => null,
        "HONORIFIC" => null,
        "NAME" => "name",
        "SECOND_NAME" => "second_name",
        "LAST_NAME" => "last_name",
        "PHOTO" => null,
        "LEAD_ID" => null,
        "TYPE_ID" => null,
        "SOURCE_ID" => null,
        "SOURCE_DESCRIPTION" => null,
        "COMPANY_ID" => null,
        "BIRTHDATE" =>  'birthdate',
        "EXPORT" => null,
        "HAS_PHONE" => null,
        "HAS_EMAIL" => null,
        "HAS_IMOL" => null,
        "DATE_CREATE" => null,
        "DATE_MODIFY" => null,
        "ASSIGNED_BY_ID" => null,
        "CREATED_BY_ID" => null,
        "MODIFY_BY_ID" => null,
        "OPENED" => null,
        "ORIGINATOR_ID" => null,
        "ORIGIN_ID" => null,
        "ORIGIN_VERSION" => null,
        "FACE_ID" => null,
        "ADDRESS" => 'address',
        "ADDRESS_2" => null,
        "ADDRESS_CITY" => null,
        "ADDRESS_POSTAL_CODE" => null,
        "ADDRESS_REGION" => null,
        "ADDRESS_PROVINCE" => null,
        "ADDRESS_COUNTRY" => null,
        "ADDRESS_LOC_ADDR_ID" => null,
        "UTM_SOURCE" => null,
        "UTM_MEDIUM" => null,
        "UTM_CAMPAIGN" => null,
        "UTM_CONTENT" => null,
        "UTM_TERM" => null,
        "LAST_ACTIVITY_BY" => null,
        "LAST_ACTIVITY_TIME" => null
    ];
}

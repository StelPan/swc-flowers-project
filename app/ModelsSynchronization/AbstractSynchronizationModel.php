<?php

namespace App\ModelsSynchronization;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;

abstract class AbstractSynchronizationModel
{
    /**
     * BITRIX_MODEL.FIELD => LOCAL_MODEL.FIELD
     * @var array
     */
    protected array $fieldsAssociation;

    /**
     * @param Model $model
     * @param array $options
     * @return bool
     */
    public function sync(Model $model, array $options): bool
    {
        foreach ($options as $field => $value) {
            if (!array_key_exists($field, $this->fieldsAssociation) || $this->fieldsAssociation[$field] === null) {
                continue;
            }

            $model->{$this->fieldsAssociation[$field]} = $value;
        }
        return $model->save();
    }

    public function create(Model $instance, array $options, array $owners = []): Model
    {
        foreach($options as $field => $value) {
            if (!array_key_exists($field, $this->fieldsAssociation) || $this->fieldsAssociation[$field] === null) {
                continue;
            }

            $instance->{$this->fieldsAssociation[$field]} = $value;
        }

        foreach ($owners as $key => $value) {
            $instance->{$key} = $value;
        }

        $instance->save();

        return $instance;
    }
}

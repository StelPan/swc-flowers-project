<?php

namespace App\ModelsSynchronization;

class UserSynchronizationModel extends AbstractSynchronizationModel
{
    protected array $fieldsAssociation = [
        'ID' => 'bitrix_id',
        'EMAIL' => 'email',
        'NAME' => 'name',
        'PERSONAL_MOBILE' => 'personal_mobile',
    ];
}
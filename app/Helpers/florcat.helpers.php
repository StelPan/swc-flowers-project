<?php

if (!function_exists('florcat_calculate_expense_bonus')) {
    function florcat_calculate_expense_bonus(float $opportunity, int $balance): int
    {
        $half = $opportunity / 2;
        return floor($half <= $balance ? $half :  0);
    }
}

if (!function_exists('florcat_is_not_correct_bonus')) {
    function florcat_is_not_correct_bonus(int $bonus, float $opportunity, int $balance): bool
    {
        return ($bonus > florcat_calculate_expense_bonus($opportunity, $balance)) || ($bonus > $balance);
    }
}
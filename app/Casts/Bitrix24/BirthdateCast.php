<?php

namespace App\Casts\Bitrix24;

use Illuminate\Contracts\Database\Eloquent\CastsAttributes;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\File;

class BirthdateCast implements CastsAttributes
{
    /**
     * Cast the given value.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @param  string  $key
     * @param  mixed  $value
     * @param  array  $attributes
     * @return mixed
     */
    public function get($model, string $key, $value, array $attributes)
    {
        return $value;
    }

    /**
     * Prepare the given value for storage.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @param  string  $key
     * @param  mixed  $value
     * @param  array  $attributes
     */
    public function set($model, string $key, $value, array $attributes)
    {
        if (!strlen($value)) {
            return null;
        }

        $carbon = Carbon::make($value);

        return ($carbon->year . '-' . $carbon->month . '-' . $carbon->day);
    }
}

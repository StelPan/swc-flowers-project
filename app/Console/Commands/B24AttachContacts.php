<?php

namespace App\Console\Commands;

use App\Models\Contact;
use App\ModelsSynchronization\ContactSynchronizationModel;
use App\Services\Bitrix24\BatchService;
use Bitrix24\SDK\Core\ApiClient;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class B24AttachContacts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'b24:attach-contacts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Выгружает все контакты из Б24 и создает контакты в локальной базе';

    /**
     * Execute the console command.
     *
     * @param ApiClient $api
     * @param ContactSynchronizationModel $sync
     * @return int
     */
    public function handle(
        ApiClient                   $api,
        ContactSynchronizationModel $sync,
        BatchService                $batchService,

    ): int
    {
        try {
            DB::beginTransaction();

            $response = $api->getResponse('crm.contact.list')->toArray();

            if (!((int)$response['total'])) {
                print 'Контакты в Битрикс24 не найдены.';

                return Command::SUCCESS;
            }

            printf('Всего контактов обнаружено: %d', $response['total']);
            print("\r\n");

            $batchPages = ceil($response['total'] / 50);
            $totalContacts = $response['total'];
            $counter = 1;

            print "Начало выгрузки контактов.. \r\n";

            for ($batchPage = 1; $batchPage <= $batchPages; $batchPage++) {
                printf("Осталось контактов %d", $totalContacts);
                print "\r\n";

                $response = $api->getResponse('crm.contact.list', [
                    'select' => ['ID', 'NAME', 'LAST_NAME', 'SECOND_NAME', 'PHONE', 'EMAIL', 'BIRTHDATE'],
                    'start' => $counter
                ])->getContent(true);

                $contacts = json_decode($response, true)['result'];


                foreach ($contacts as $contact) {
                    $sync->create(
                        new Contact(),
                        $contact
                    );
                }

                $totalContacts -= 50;
                $counter += 50;

                sleep(1);
            }

            DB::commit();
            return Command::SUCCESS;
        } catch (ClientException $exception) {
            DB::rollBack();

            return Command::FAILURE;
        }
    }

    public function commands(int $pages, int $counter): array
    {
        $batch = [];

        for ( ; $counter <= $pages; $counter += 50) {
            $batch[] = 'crm.contact.list?' . http_build_query([
                    'select' => ['*'],
                    'start' => $counter
                ]);
        }

        return $batch;
    }
}

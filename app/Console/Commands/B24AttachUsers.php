<?php

namespace App\Console\Commands;

use App\Models\User;
use App\ModelsSynchronization\UserSynchronizationModel;
use App\Services\Bitrix24\BatchService;
use App\Services\Bitrix24\ImService;
use App\Services\Bitrix24\UserService;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class B24AttachUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'b24:attach-users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Выгружает всех сотрудников из Б24 и создает пользователей';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(
        ImService $imService,
        UserService $userService,
        BatchService $batchService,
        UserSynchronizationModel $synchronizationModel
    ) {
        try {
            $commandMessages = [];

            $users = $userService->get();
            if (!(int)$users['total']) {
                print('Пользователи в Б24 отсутствуют.');

                return Command::SUCCESS;
            }

            print ("Всего пользователей: " . $users['total'] . "\r\n");

            $commands = $this->commands(ceil($users['total'] / 50));
            $batch = $batchService->batch($commands)['result'];

            DB::beginTransaction();
            foreach ($batch as $key => $value) {
                foreach ($value as $user) {
                    if ($user['ACTIVE']) {
                        if (empty($user['EMAIL'])) {
                            $user = array_merge($user, ['EMAIL' => 'temp-florcat' . $user['ID'] . '@mail.ru']);
                        }

                        if (!User::whereEmail($user['EMAIL'])->first()) {
                            $instance = new User();
                            $password = Str::random(8);
                            $options = ['password' => Hash::make($password)];

                            $create = $synchronizationModel->create(
                                $instance,
                                $user,
                                $options
                            );

                            if (config('app.env') === 'production') {
                                $commandMessages[$user['ID']] = 'im.message.add?' . http_build_query([
                                        'DIALOG_ID' => $user['ID'],
                                        'MESSAGE' => config('app.name') . ".\r\n"
                                            . config('app.url') . "\r\n"
                                            . "Данные для входа в учетную запись сотрудника: \r\n"
                                            . "Логин: " . $user['EMAIL'] . "\r\n"
                                            . "Пароль: " . $password
                                    ]);
                            }
                            $instance->assignRole('administrator');
                            try {
                                Mail::to($instance)->send(new \App\Mail\UserCreatedMail(login: $user['EMAIL'], password: $password));
                    } catch (\Exception $exception) {
                                Log::error(['Notify new user' => $exception->getMessage()]);
                            }
                        }
                    }
                }
            }

            print "Начало отправки сообщений сотрудникам.. \r\n";

            if (config('app.env') === 'production') {
                $batchService->batch($commandMessages);
            }

            print 'Сообщения отправлены..';

            DB::commit();
            return Command::SUCCESS;
        } catch (ClientException $exception) {
            DB::rollBack();

            return Command::FAILURE;
        }
    }

    private function commands($pages): array
    {
        $commands = [];

        for ($i = 1; $i <= $pages; $i++) {
            $commands['get_' . $i] = 'user.get?' . http_build_query([
                    'select' => ['*'],
                    'start' => '$next[get_' . ($i - 1) . ']',
                ]);
        }

        return $commands;
    }
}

<?php

namespace App\Services\Database;

use App\Models\Role;

class RoleService
{
    public function all()
    {
        return Role::all();
    }
}
<?php

namespace App\Services\Database;

use App\Models\User;

class UserService
{
    public function __construct()
    {
        //
    }

    public function get(int $id)
    {
        return User::with('roles')->findOrFail($id);
    }
}
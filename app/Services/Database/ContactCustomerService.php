<?php

namespace App\Services\Database;

use App\Repositories\Database\ContactRepository;
use App\Repositories\Florcat\CustomerRepository;
use Illuminate\Http\Request;
use Spatie\FlareClient\Http\Exceptions\NotFound;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ContactCustomerService
{
    private ContactRepository $contactRepository;

    private CustomerRepository $customerRepository;

    public function __construct(ContactRepository $contactRepository, CustomerRepository $customerRepository)
    {
        $this->contactRepository = $contactRepository;

        $this->customerRepository = $customerRepository;
    }

    /**
     * @param Request $request
     * @return void
     */
    public function create(Request $request)
    {
        $this->contactRepository->create($request->all());
    }

    /**
     * @throws NotFound
     */
    public function search(array $params)
    {
        $customer = $contact = null;

        if (!empty($params['florcat'])) {
            $customer = $this
                ->customerRepository
                ->findById((int) $this->getCustomerIdFromUrl($params['florcat']))['data'];
        }

        if (!empty($params['phone'])) {
            $customer = $this
                ->customerRepository
                ->findByPhone($params['phone']);

            if (!$customer['data']['pagination']['count']) {
                throw new NotFound('Пользователь не найден в Florcat сервисе', 404);
            }

            $customer = $customer['data']['data'][0];
        }

        $contact = $this
            ->contactRepository
            ->findByPhone(trim($customer['phone'], '+'))
            ->first();

        if (!$contact) {
            throw new NotFound('Пользователь найден в сервисе Florcat, но не найден Б24.');
        }

        // Обновляем контакт если в нет отсутствует идентификатор из Florcat
        if (!$contact->florcat_contact_id) {
            $this->updateContact($contact->id, ['florcat_contact_id' => $customer['id']]);
        }

        $contact = $contact->toArray();
        $contact['florcat'] = $customer;

        return $contact;
    }

    public function findContactById(int $id)
    {
        $contact = $this->contactRepository->findById($id);
        if (!$contact) {
            throw new NotFoundHttpException('Контакт не найден в базе данных.');
        }

        return $this->search(['phone' => $contact['phone']]);
    }

    /**
     * @param int $id
     * @param array $params
     * @return mixed
     */
    public function updateContact(int $id, array $params): mixed
    {
        return $this->contactRepository->update($id, $params);
    }

    private function getCustomerIdFromUrl(string $url): string
    {
        $segments = explode('/', parse_url($url, PHP_URL_PATH));
        return $segments[count($segments) - 2];
    }
}

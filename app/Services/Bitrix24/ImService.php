<?php

namespace App\Services\Bitrix24;

use Bitrix24\SDK\Core\ApiClient;

class ImService
{
    private ApiClient $client;

    public function __construct(ApiClient $client)
    {
        $this->client = $client;
    }

    public function add(array $options = [])
    {
        return $this->client->getResponse('im.message.add', $options)->toArray();
    }
}
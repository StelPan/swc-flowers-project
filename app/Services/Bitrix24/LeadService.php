<?php

namespace App\Services\Bitrix24;

use App\Repositories\LeadRepository;
use App\Services\Database\ContactCustomerService;

class LeadService
{
    private LeadRepository $leadRepository;

    private ContactCustomerService $contactCustomerService;

    private array $userFields;

    public function __construct(LeadRepository $leadRepository, ContactCustomerService $contactCustomerService)
    {
        $this->leadRepository = $leadRepository;

        $this->contactCustomerService = $contactCustomerService;

        $this->userFields = config('bitrix.user_fields.leads');
    }

    /**
     * @throws \Exception
     */
    public function add(array $params)
    {
        $contact = $this
            ->contactCustomerService
            ->findContactById($params['contact_id']);

        if (!empty($params['bonus'])) {
            $isCorrect = $this->validateBonus(
                $params['bonus'],
                $params['opportunity'],
                $contact['florcat']['bonus_balance']
            );

            if (!$isCorrect) {
                throw new \Exception(
                    '1) Бонус должен составлять 50% от суммы покупки округленный в меньшую сторону (до рубля).'.
                    '2) Бонус для списание не может быть больше баланса клиента.'
                );
            }
        }

        $fields = [
            'TITLE' => 'Заказ проходка из приложения',
            'ASSIGNED_BY_ID' => auth()->user()->bitrix_id,
            'CONTACT_ID' => $contact['bitrix_contact_id'],
            'OPPORTUNITY' => $params['opportunity'],
            $this->userFields['BONUS'] => $params['bonus'],
        ];

        $id = $this
            ->leadRepository
            ->add(['fields' => $fields])['result'];

        $this->update(['id' => $id, 'fields' => ['TITLE' => 'Заказ проходка из приложения №' . $id]]);

        return $id;
    }

    public function update(array $params): array
    {
        return $this->leadRepository->update($params);
    }

    private function validateBonus(int $bonus, float $opportunity, int $balance): bool
    {
        return florcat_calculate_expense_bonus($bonus, $opportunity, $balance);
    }
}
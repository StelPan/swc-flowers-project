<?php

namespace App\Services\Bitrix24;

use Bitrix24\SDK\Core\ApiClient;

class UserService
{
    private $client;

    public function __construct(ApiClient $client)
    {
        $this->client = $client;
    }

    public function get(array $params = [])
    {
         return $this->client->getResponse('user.get', $params)->toArray();
    }
}

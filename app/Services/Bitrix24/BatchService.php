<?php

namespace App\Services\Bitrix24;

use Bitrix24\SDK\Core\ApiClient;

class BatchService
{
    private ApiClient $client;

    public function __construct(ApiClient $client)
    {
        $this->client = $client;
    }

    public function batch(array $commands = [], int $halt = 0)
    {
        return $this->client->getResponse('batch', [
            'halt' => $halt,
            'cmd' => $commands
        ])->toArray()['result'];
    }
}

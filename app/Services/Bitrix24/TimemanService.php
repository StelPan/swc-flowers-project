<?php

namespace App\Services\Bitrix24;

use Bitrix24\SDK\Core\ApiClient;
use Carbon\Carbon;

class TimemanService
{
    private ApiClient $client;

    public function __construct(ApiClient $client)
    {
        $this->client = $client;
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function pause(int $id)
    {
        return $this->request('timeman.pause', $id);
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function close(int $id)
    {
        return $this->request('timeman.close', $id, ['report' => 'test', ['time' => Carbon::now()]]);
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function status(int $id)
    {
        return $this->request('timeman.status', $id);
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function open(int $id)
    {
        return $this->request('timeman.open', $id);
    }

    /**
     * @param string $method
     * @param int $id
     * @return array
     * @throws \Bitrix24\SDK\Core\Exceptions\InvalidArgumentException
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    private function request(string $method, int $id, array $params = [])
    {
        return $this->client->getResponse($method, array_merge(['user_id' => $id], $params))->toArray();
    }
}

<?php

namespace App\Services\Bitrix24;

use Bitrix24\SDK\Core\ApiClient;
use Illuminate\Support\Facades\Auth;

class TaskService
{
    private ApiClient $client;

    public static $LIST = 'tasks.task.list';

    public function __construct(ApiClient $client)
    {
        $this->client = $client;
    }

    public function get(int $id, array $select = ['UF_CRM_TASK', 'STATUS', '*'])
    {
        $task = $this->client
            ->getResponse('tasks.task.get', ['id' => $id, 'select' => $select])
            ->toArray()['result'];

        return $task;
    }

    public function toComplete(array $params)
    {
        return $this->client
            ->getResponse('tasks.task.complete', $params)
            ->toArray()['result'];
    }

    public function toStart(array $params)
    {
        return $this->client
            ->getResponse('tasks.task.start', $params)
            ->toArray()['result'];
    }

    public function list(array $params)
    {

        $tasks = $this->client
            ->getResponse(static::$LIST, $params)
            ->toArray();

        return $tasks;
    }
}

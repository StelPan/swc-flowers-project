<?php

namespace App\Services\Bitrix24;

use Bitrix24\SDK\Core\ApiClient;
use Bitrix24\SDK\Core\Exceptions\InvalidArgumentException;
use Illuminate\Support\Collection;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class DealService
{
    private ApiClient $client;

    private BatchService $batchService;

    private TaskService $taskService;

    private array $customFields;

    public function __construct(ApiClient $client, BatchService $batchService, TaskService $taskService)
    {
        $this->client = $client;

        $this->batchService = $batchService;

        $this->taskService = $taskService;

        $this->customFields = config('bitrix.user_fields.deals');
    }

    public function get(array $params = [])
    {
        $deal = $this
            ->client
            ->getResponse('crm.deal.get', $params)
            ->toArray()['result'];

        if (!$deal) {
            throw new NotFoundHttpException('Сделка не найдена в Б24');
        }

        return $deal;
    }

    public function getDealByTaskId(int $id): array
    {
        $task = $this->taskService->get($id);
        if (empty($task['task'])) {
            throw new NotFoundHttpException('Задача в Б24 не найдена');
        }

        $task = $task['task'];

        $dealRelation = collect($task['ufCrmTask'] ?? [])
            ->first(fn($relation) => preg_match('/^D_(.*)$/', $relation));

        if (!$dealRelation) {
            throw new NotFoundHttpException('Сделка в Б24 не найдена');
        }

        $dealId = explode("_", $dealRelation)[1];

        $deal = $this->get(['id' => $dealId]);

        $batchRequest = $this->batchService->batch([
            'PRODUCTS' => 'crm.deal.productrows.get?' . http_build_query(['id' => $dealId]),
            'CREATOR' => 'user.get?' . http_build_query(['id' => $deal['CREATED_BY_ID']]),
            'CONTACT' => 'crm.contact.get?' . http_build_query(['id' => $deal['CONTACT_ID']]),
        ])['result'];

        $deal['PRODUCTS'] = $batchRequest['PRODUCTS'];
        $deal['CONTACT'] = $batchRequest['CONTACT'] ?? [];
        $deal['CREATOR'] = $batchRequest['CREATOR'][0];

        $deal['TASK'] = $task;

        $deal['CUSTOM_FIELDS'] = $this->customFields;

        $batchArray = [];
        Collection::make($batchRequest['PRODUCTS'])->each(function ($product) use (&$batchArray) {
            $batchArray[$product['PRODUCT_ID']] = 'catalog.productImage.list?' . http_build_query([
                    'productId' => $product['PRODUCT_ID'],
                ]);
        });

        $batchRequestProductImages = $this->batchService->batch($batchArray)['result'];

        foreach ($deal['PRODUCTS'] as $index => $PRODUCT) {
            $deal['PRODUCTS'][$index]['IMAGES'] = [];
        }

        foreach ($batchRequestProductImages as $ikey => $image) {
            foreach ($deal['PRODUCTS'] as $pkey => $product) {
                $deal['PRODUCTS'][$pkey]['IMAGES'] = [];
                if ((int)$product['PRODUCT_ID'] === $ikey) {
                    $deal['PRODUCTS'][$pkey]['IMAGES'] = $image['productImages'];
                }
            }
        }

        return $deal;
    }

    public function store(array $params = [])
    {
        return $this->client->getResponse('tasks.task.add', $params)->toArray()['result'];
    }

    public function update(int $id, array $params = []): array
    {
        return $this->client->getResponse(
            'crm.deal.update',
            array_merge(['id' => $id], $params)
        )->toArray();
    }
}

<?php

namespace App\Repositories\Database;

use App\Models\Contact;

class ContactRepository
{
    private Contact $model;

    public function __construct(Contact $contact)
    {
        $this->model = $contact;
    }

    public function findById(int $id)
    {
        return $this->model->find($id);
    }

    public function findByPhone(string $phone)
    {
        return $this->model->where('phone', 'like', '%' . $phone . '%');
    }

    public function findByFlorcatCustomerId(int $id)
    {
        return $this->model->where('florcat_contact_id', '=', $id)->first();
    }

    public function create(array $params): Contact
    {
        return $this->model->create($params);
    }

    public function update(int $id, array $params)
    {
        $contact = $this->model->findOrFail($id);
        return $contact->update($params);
    }
}

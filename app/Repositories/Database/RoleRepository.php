<?php

namespace App\Repositories\Database;

use App\Models\Role;

class RoleRepository
{
    public function all()
    {
        return Role::all();
    }
}

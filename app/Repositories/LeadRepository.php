<?php

namespace App\Repositories;

use Bitrix24\SDK\Core\ApiClient;

class LeadRepository
{
    private ApiClient $client;

    public function __construct(ApiClient $client)
    {
        $this->client = $client;
    }

    public function add(array $params)
    {
        return $this->client->getResponse('crm.lead.add', $params)->toArray();
    }

    public function update(array $params)
    {
        return $this->client->getResponse('crm.lead.update', $params)->toArray();
    }
}
<?php

namespace App\Repositories\Florcat;

use App\Clients\Florcat;

class CustomerRepository
{
    private Florcat $client;

    public function __construct(Florcat $client)
    {
        $this->client = $client;
    }

    public function findById(int $id)
    {
        return $this->client->getCustomerById($id);
    }

    public function findByPhone(string $phone)
    {
        return $this->client->getCustomerByPhone($phone);
    }
}
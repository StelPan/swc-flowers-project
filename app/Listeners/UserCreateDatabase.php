<?php

namespace App\Listeners;

use App\Events\Bitrix24\UserCreated;
use App\Models\User;
use App\ModelsSynchronization\AbstractSynchronizationModel;
use App\ModelsSynchronization\UserSynchronizationModel;
use App\Services\Bitrix24\ImService;
use App\Services\Bitrix24\UserService;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class UserCreateDatabase
{
    private UserService $userService;

    private ImService $imService;

    private UserSynchronizationModel $model;

    public function __construct(UserService $userService, ImService $imService, UserSynchronizationModel $model)
    {
        $this->userService = $userService;
        $this->imService = $imService;
        $this->model = $model;
    }

    /**
     * Handle the event.
     *
     * @param UserCreated $event
     * @return void
     */
    public function handle(UserCreated $event): void
    {
        try {
            $id = $event->data['data']['ID'];

            $user = $this->userService->get(['ID' => $id])['result'][0];

            $password = $this->passwordWithoutHash(10);

            $instance = new User();

            $this->model->create(
                $instance,
                $user,
                ['password' => Hash::make($password)]
            );

            if (config('app.env') === 'production') {
                $this->imService->add([
                    'DIALOG_ID' => $user['ID'],
                    'MESSAGE' => config('app.name') . ".\r\n"
                        . config('app.url') . "\r\n"
                        . "Данные для входа в учетную запись сотрудника: \r\n"
                        . "Логин: " . $user['EMAIL'] . "\r\n"
                        . "Пароль: " . $password
                ]);
            }

            try {
                Mail::to($instance)->send(new \App\Mail\UserCreatedMail(login: $user['EMAIL'], password: $password));
            } catch (\Exception $exception) {
                Log::error(['Notify new user' => $exception->getMessage()]);
            }


            Log::debug('EVENT:ONUSERADD ' . json_encode($user, JSON_THROW_ON_ERROR));
        } catch (\Exception $exception) {
            Log::critical('EVENT:ONUSERADD ' . $exception->getMessage());
        }
    }

    public function passwordWithoutHash (int $length): string
    {
        return Str::random($length);
    }
}

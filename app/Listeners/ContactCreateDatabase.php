<?php

namespace App\Listeners;

use App\Events\Bitrix24\ContactCreated;
use App\Models\Contact;
use App\ModelsSynchronization\ContactSynchronizationModel;
use Bitrix24\SDK\Core\ApiClient;
use Illuminate\Support\Facades\Log;

class ContactCreateDatabase
{
    private ApiClient $api;

    private ContactSynchronizationModel $synchronizationModel;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(ApiClient $api, ContactSynchronizationModel $synchronizationModel)
    {
        $this->api = $api;

        $this->synchronizationModel = $synchronizationModel;
    }

    /**
     * Handle the event.
     *
     * @param ContactCreated $event
     * @return void
     */
    public function handle(ContactCreated $event)
    {
        try {
            $id = $event->data['data']['FIELDS']['ID'];
            $contact = $this->api->getResponse('crm.contact.get', ['id' => $id])->toArray()['result'];

            $instance = new Contact;
            $this->synchronizationModel->create($instance, $contact);

            Log::debug('EVENT:ONCRMCONTACTADD ' . json_encode($contact, JSON_THROW_ON_ERROR));
        } catch (\Exception $exception) {
            Log::critical('EVENT:ONCRMCONTACTADD ' .  $exception->getMessage());
        }
    }
}

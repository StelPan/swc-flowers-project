<?php

namespace App\Listeners;

use App\Events\Bitrix24\ContactUpdated;
use App\Models\Contact;
use App\ModelsSynchronization\AbstractSynchronizationModel;
use App\ModelsSynchronization\ContactSynchronizationModel;
use Bitrix24\SDK\Core\ApiClient;
use Illuminate\Support\Facades\Log;

class ContactUpdateDatabase
{
    private ApiClient $api;

    private AbstractSynchronizationModel $synchronizationModel;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(ApiClient $api, ContactSynchronizationModel $synchronizationModel)
    {
        $this->api = $api;

        $this->synchronizationModel = $synchronizationModel;
    }

    /**
     * Handle the event.
     *
     * @param ContactUpdated $event
     * @return false
     */
    public function handle(ContactUpdated $event): bool
    {
        try {
            $id = $event->data['data']['FIELDS']['ID'];
            $contact = $this->api->getResponse('crm.contact.get', ['id' => $id])->toArray()['result'];

            $model = Contact::where('bitrix_contact_id', $id)->first();
            if(!$model) {
                Log::warning('EVENT:ONCRMCONTACTUPDATE NOT FOUND BITRIX ID' . $id);

                return false;
            }

            $this->synchronizationModel->sync($model, $contact);

            Log::debug('EVENT:ONCRMCONTACTUPDATE' . json_encode($contact, JSON_THROW_ON_ERROR));
            return true;
        } catch (\Exception $exception) {
            Log::critical('EVENT:ONCRMCONTACTUPDATE' . $exception->getMessage());

        }
    }
}

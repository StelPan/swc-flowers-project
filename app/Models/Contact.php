<?php

namespace App\Models;

use App\Casts\Bitrix24\BirthdateCast;
use App\Casts\Bitrix24\PhoneCast;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Request;

class Contact extends Model
{
    use HasFactory;

    protected $fillable = [
        'bitrix_contact_id',
        'florcat_contact_id',
        'name',
        'second_name',
        'last_name',
        'full_name',
        'phone',
        'address',
        'birthdate',
    ];

    protected $casts = [
        'birthdate' => BirthdateCast::class,
        'phone' => PhoneCast::class,
    ];

    public static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            $model->full_name = trim($model->last_name . ' ' . $model->name . ' ' . $model->second_name);
        });
    }

    public function bonus()
    {
        return $this->hasOne(BonusTransaction::class)->orderBy('id', 'desc');
    }

    public function scopeOfPhone(Builder $builder, array $params): Builder
    {
        if (empty($params['phone'])) {
            return $builder;
        }

        return $builder->where('phone', '=', $params['phone']);
    }

    public function scopeOfId(Builder $builder, array $params): Builder
    {
        if (empty($params['id'])) {
            return $builder;
        }

        return $builder->where('id', '=', (int) $params['id']);
    }
}

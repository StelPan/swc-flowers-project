<?php

namespace App\Providers;

use App\Events\Bitrix24\ContactCreated;
use App\Events\Bitrix24\ContactUpdated;
use App\Events\Bitrix24\LeadCreated;
use App\Events\Bitrix24\LeadUpdated;
use App\Events\Bitrix24\UserCreated;
use App\Listeners\ContactCreateDatabase;
use App\Listeners\ContactUpdateDatabase;
use App\Listeners\LeadCreateDatabase;
use App\Listeners\LeadUpdateDatabase;
use App\Listeners\UserCreateDatabase;
use App\Models\Lead;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        ContactUpdated::class => [
            ContactUpdateDatabase::class,
        ],
        ContactCreated::class => [
            ContactCreateDatabase::class,
        ],
        UserCreated::class => [
            UserCreateDatabase::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}

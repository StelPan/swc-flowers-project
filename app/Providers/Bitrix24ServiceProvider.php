<?php

namespace App\Providers;

use Bitrix24\SDK\Core\ApiClient;
use Bitrix24\SDK\Core\Credentials\Credentials;
use Bitrix24\SDK\Core\Credentials\WebhookUrl;
use Illuminate\Support\ServiceProvider;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Symfony\Component\HttpClient\HttpClient;

class Bitrix24ServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(ApiClient::class, function ($app) {
            $credentials = new Credentials(
                new WebhookUrl(config('bitrix.webhook')),
                null,
                null,
                null
            );

            $log = new Logger('name');
            $log->pushHandler(new StreamHandler('b24-api-client-debug.log', Logger::DEBUG));

            $client = HttpClient::create();

            return new ApiClient($credentials, $client, $log);
        });
    }
}

<?php

namespace App\Types;

class RoleTypes
{
    public static string $CACHIER = 'cachier';

    public static string $GROWER = 'grower';

    public static string $PACKER = 'packer';

    public static string $COURIER = 'courier';

    public static string $LOGISTICIAN = 'logistician';

    public static string $ADMINISTRATOR = 'administrator';
}

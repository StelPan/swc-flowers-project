<?php

namespace App\Types\User;

class Type
{
    public static $EMPLOYEE = 'employee';

    public static $EXTRANET = 'extranet';

    public static $EMAIL = 'email';
}
<?php

namespace App\Types;

class TaskStatusTypes
{
    public static $WAIT_COMPLETE = 2;

    public static $PROCESSED = 3;

    public static $WAIT_CONTROL = 4;

    public static $COMPLETED = 5;

    public static $POSTPONED = 6;
}

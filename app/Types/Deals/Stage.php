<?php

namespace App\Types\Deals;

class Stage
{
    public static $NEW = 'NEW';

    public static $PREPARATION = 'PREPARATION';

    public static $PREPAYMENT_INVOICE = 'PREPAYMENT_INVOICE';

    public static $EXECUTING = 'EXECUTING';

    public static $FINAL_INVOICE = 'FINAL_INVOICE';
}
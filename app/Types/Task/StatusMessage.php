<?php

namespace App\Types\Task;

class StatusMessage
{
    public static array $message = [
        2 => 'Ждет выполнения',
        3 => 'Выполняется',
        4 => 'Ожидает контроля',
        5 => 'Завершена',
        6 => 'Отложена'
    ];

    public static function getMessage($code)
    {
        return self::$message[$code];
    }
}

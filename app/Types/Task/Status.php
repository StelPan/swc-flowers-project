<?php

namespace App\Types\Task;

class Status
{
    public static $STATE_NEW = 1;

    public static $STATE_PENDING = 2;

    public static $STATE_IN_PROGRESS = 3;

    public static $STATE_SUPPOSEDLY_COMPLETED = 4;

    public static $STATE_COMPLETED = 5;

    public static $STATE_DEFERRED = 6;

    public static $STATE_DECLINED = 7;
}
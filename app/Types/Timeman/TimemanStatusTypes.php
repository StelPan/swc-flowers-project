<?php

namespace App\Types\Timeman;

class TimemanStatusTypes
{
    public static string $OPENED = 'OPENED';

    public static string $CLOSED = 'CLOSED';

    public static string $PAUSED = 'PAUSED';

    public static string $EXPIRED = 'EXPIRED';
}

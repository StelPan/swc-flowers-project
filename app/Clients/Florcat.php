<?php

namespace App\Clients;

use GuzzleHttp\Client;

class Florcat
{
    private Client $client;

    private array $config;

    public function __construct()
    {
        $this->config = config('florcat');

        $this->client = new Client([
            'headers' => [
                'Accept' => 'application/json',
                'mobile-app' => 'florcat_employee',
            ]
        ]);
    }

    public function getCustomerById(int $id)
    {
        $response = $this
            ->client
            ->request('GET', $this->config['url'] . '/api/v1/employee/customer/' . $id . '/info');

        return json_decode($response->getBody()->getContents(), true);
    }

    public function getCustomerByPhone(string $phone)
    {
        $response = $this
            ->client
            ->request('GET', $this->config['url'] . '/api/v1/employee/customer?phone=' . $phone);

        return json_decode($response->getBody()->getContents(), true);
    }
}
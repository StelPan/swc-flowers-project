<?php

namespace App\Clients;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class FlorcatCrm
{
    private Client $client;

    private array $config;

    public function __construct()
    {
        $this->client = new Client();
        $this->config = config('bitrix');
    }

    /**
     * @param $dealId
     * @param $userId
     * @return mixed
     * @throws GuzzleException
     * @throws \JsonException
     */
    public function newTask($dealId, $userId): mixed
    {
        $params = [
            'deal_id' => $dealId,
            'user_id' => $userId,
            'secretcode' => $this->config['secret_code']
        ];

        $response = $this->client->request('POST', $this->config['base_url'] . '/local/newtask/', [
            'form_params' => $params
        ]);

        return json_decode(
            $response->getBody()->getContents(),
            true,
            512,
            JSON_THROW_ON_ERROR
        );
    }
}

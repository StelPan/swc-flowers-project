<?php

namespace App\Clients;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class Wazzup
{
    private $client;

    private $apiKey;

    private $apiURI = 'https://api.wazzup24.com/v3/iframe';

    public function __construct(Client $client)
    {
        $this->client = $client;

        $this->apiKey = config('wazzup.apiKey');
    }

    public function iframe($id)
    {
        return $this->client->request('post', $this->apiURI, [
            'form_data' => [
                'user' => [
                    'id' => $id,
                    'name' => 'name'
                ],
                'scope' => 'global'
            ],
            'headers' => [
                'Authorization' => 'Bearer ' . $this->apiKey
            ]
        ])->getBody()->getContents();
    }
}
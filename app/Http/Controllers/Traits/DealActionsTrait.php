<?php

namespace App\Http\Controllers\Traits;

use App\Http\Requests\deal\UpdateDocketRequest;
use App\Http\Requests\deal\UpdateTrolAndSheflRequest;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpClient\Exception\ClientException;

trait DealActionsTrait
{
    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(int $id)
    {
        try {
            $deal = $this->dealService->get(['ID' => $id]);

            $batch = $this->batchService->batch([
                'ASSIGNED' => 'user.get?' . http_build_query(['id' => $deal['ASSIGNED_BY_ID']]),
                'PRODUCTS' => 'crm.deal.productrows.get?' . http_build_query(['id' => $id]),
                'TASKS' => 'tasks.task.list?' . http_build_query(['filter' => ['UF_CRM_TASK' => 'D_' . $id]]),
            ]);

            foreach ($batch['result'] as $key => $value) {
                $deal[$key] = $value;
            }

            $deal['CUSTOM_FIELDS'] = config('bitrix.user_fields.deals');

            return response()->json($deal, 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Сделка не найдена'], $e->getCode() < 400 ? 400 : $e->getCode());
        }
    }

    /**
     * @param int $id
     * @param UpdateDocketRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function docket(int $id, UpdateDocketRequest $request): \Illuminate\Http\JsonResponse
    {
        try {
            $validated = $request->all();

            $deal = $this->dealService->get(['ID' => $id]);
            if (!$deal) {
                return response()->json(['message' => 'Сделка не найдена'], 404);
            }

            $response = $this->dealService->update($id, ['fields' => [
                config('bitrix.user_fields.deals.DOCKET') => $validated['docket']
            ]]);

            return response()->json($response, 200);
        } catch (ClientException $exception) {
            return response()->json(['message' => $exception->getResponse()->getContent(true)], 400);
        } catch (\Exception $exception) {
            return response()->json(['message' => $exception->getMessage()], $exception->getCode());
        }
    }

    /**
     * Выполняет установку троли и полки, если уже устновлено, то перезаписывает
     *
     * @param int $id
     * @param UpdateTrolAndSheflRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storage(int $id, UpdateTrolAndSheflRequest $request): \Illuminate\Http\JsonResponse
    {
        try {
            $isRewriteStorage = false;

            $validated = $request->validated();

            $deal = $this->dealService->get(['ID' => $id]);
            if (!$deal) {
                return response()->json(['message' => 'Сделка не найдена'], 404);
            }

            if ($deal[config('bitrix.user_fields.deals.TROL_AND_SHEFL')]) {
                $isRewriteStorage = true;
            }

            $fields = [config('bitrix.user_fields.deals.TROL_AND_SHEFL') => $validated['storage']];

            $this->dealService->update($id, ['fields' => $fields]);

            $deal = array_merge(
                ['IS_REWRITE_STORAGE' => $isRewriteStorage],
                ['USER_FIELDS' => config('bitrix.user_fields.deals')],
                $deal,
                $fields,
            );

            return response()->json($deal, 200);
        }catch (\Exception $exception) {
            Log::error(__CLASS__ . '::' . __METHOD__ . ' ' . 'message: ' . $exception->getMessage());
            return response()->json(
                ['message' => $exception->getMessage()],
                max($exception->getCode(), 400)
            );
        }
    }
}

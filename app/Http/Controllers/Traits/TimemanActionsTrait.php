<?php

namespace App\Http\Controllers\Traits;

use Carbon\Carbon;
use Illuminate\Http\Request;

trait TimemanActionsTrait
{

    public function open(Request $request): \Illuminate\Http\JsonResponse
    {
        $status = $this->request(__FUNCTION__);
        return response()->json(['status' => $status], 200);
    }

    public function close(Request $request): \Illuminate\Http\JsonResponse
    {
        $status = $this->request(__FUNCTION__);
        return response()->json(['status' => $status], 200);
    }

    public function pause(Request $request): \Illuminate\Http\JsonResponse
    {
        $status = $this->request(__FUNCTION__);
        return response()->json(['status' => $status], 200);
    }

    public function status(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            $status = $this->request(__FUNCTION__);
            return response()->json(['status' => $status], 200);
        } catch (\Exception $exception) {
            dd($exception->getMessage());
        }
    }

    private function request($method)
    {
        $result = $this->service->{$method}(auth()->user()->bitrix_id)['result'];
        $result['SERVER_TIME'] = Carbon::now();
        return $result;
    }
}

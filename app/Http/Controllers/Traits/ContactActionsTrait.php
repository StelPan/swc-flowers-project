<?php

namespace App\Http\Controllers\Traits;

use App\Http\Requests\contact\SearchContactRequest;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Log;
use Spatie\FlareClient\Http\Exceptions\NotFound;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

trait ContactActionsTrait
{
    public function get(SearchContactRequest $request): \Illuminate\Http\JsonResponse
    {
        try {
            $contact = $this->service->search($request->all());
            return response()->json([
                'error' => false,
                'data' => $contact
            ], 200);
        } catch (NotFound $exception) {
            return response()->json(['error' => false, 'message' => $exception->getMessage(), 'data' => []], 404);
        } catch (\Exception $exception) {
            Log::error(__CLASS__ . '::' . __METHOD__ . ' ' . $exception->getMessage());
            return response()->json(['message' => $exception->getMessage()], 404);
        }
    }
}

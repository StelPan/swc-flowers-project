<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class BitrixWebhookController extends Controller
{
    private array $events;

    public function __construct()
    {
        $this->events = config('bitrix.events');
    }

    public function emit(Request $request): \Illuminate\Http\JsonResponse
    {
        Log::info(['event' => $request->get('event')]);
        if (array_key_exists($request->get('event'), $this->events)) {
            $name = $this->events[$request->get('event')];
            Log::info(['$name' => $this->events[$request->get('event')]]);
            $name::dispatch($request->all());
        }

        return response()->json(['message' => 'ok'], 200);
    }
}

<?php

namespace App\Http\Controllers\Ajax;

use App\Clients\FlorcatCrm;
use App\Http\Controllers\Controller;
use App\Services\Bitrix24\BatchService;
use App\Services\Bitrix24\DealService;
use App\Services\Bitrix24\TaskService;
use Illuminate\Support\Facades\Log;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class TaskController extends Controller
{
    private TaskService $taskService;

    private FlorcatCrm $florcatCrm;

    public function __construct(
        TaskService  $taskService,
        DealService  $dealService,
        BatchService $batchService,
        FlorcatCrm $florcatCrm
    )
    {
        $this->taskService = $taskService;
        $this->dealService = $dealService;
        $this->batchService = $batchService;
        $this->florcatCrm = $florcatCrm;
    }

    public function edit(int $id): \Illuminate\Http\JsonResponse
    {
        try {
            $deal = $this->dealService->getDealByTaskId($id);
            $deal = array_merge($deal, ['BASE_URL' => config('bitrix.base_url')]);
            return response()->json($deal, 200);
        } catch (\Exception $exception) {
            return response()->json($exception->getMessage(), 400);
        }
    }

    public function start(int $id): \Illuminate\Http\JsonResponse
    {
        $result = $this->taskService->toStart(['taskId' => $id]);
        return response()->json($result, 200);
    }

    public function complete(int $id): \Illuminate\Http\JsonResponse
    {
        $result = $this->taskService->toComplete(['taskId' => $id]);
        return response()->json($result, 200);
    }

    public function setByQr(int $id): \Illuminate\Http\JsonResponse
    {
        try {
            $deal = $this->dealService->get(['ID' => $id]);
            if (!$deal) {
                throw new NotFoundHttpException('Сделка не найдена');
            }

            $response = $this->florcatCrm->newTask($id, auth()->user()->bitrix_id);
            if (isset($response['error'])) {
                throw new AccessDeniedException($response['error']);
            }

            Log::debug(__CLASS__ . '::' . __METHOD__ . ': create a new task: ' . json_encode($response));

            return response()->json($response, 200);
        } catch (\Exception $exception) {
            Log::error(__CLASS__ . '::' . __METHOD__ . ' ' . $exception->getMessage());

            return response()->json([
                'message' => $exception->getMessage()],
                422
            );
        }
    }
}

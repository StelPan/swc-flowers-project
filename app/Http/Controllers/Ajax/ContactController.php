<?php

namespace App\Http\Controllers\Ajax;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\ContactActionsTrait;
use App\Http\Requests\contact\SearchContactRequest;
use App\Services\Database\ContactCustomerService;
use GuzzleHttp\Exception\ClientException;
use Spatie\FlareClient\Http\Exceptions\NotFound;

class ContactController extends Controller
{
    use ContactActionsTrait;

    private ContactCustomerService $service;

    public function __construct(ContactCustomerService $service)
    {
        $this->service = $service;
    }
}

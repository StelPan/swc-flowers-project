<?php

namespace App\Http\Controllers\Ajax;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function updateCameraIndex($index)
    {
        auth()->user()->update(['camera_index' => $index]);

        return response()->json(['camera_index' => $index], 200);
    }
}

<?php

namespace App\Http\Controllers\Ajax;

use App\Clients\FlorcatCrm;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\DealActionsTrait;
use App\Services\Bitrix24\BatchService;
use App\Services\Bitrix24\DealService;

class DealController extends Controller
{
    use DealActionsTrait;

    private DealService $dealService;

    private BatchService $batchService;

    private FlorcatCrm $crm;

    public function __construct(
        DealService $dealService,
        BatchService $batchService,
        FlorcatCrm $crm
    )
    {
        $this->dealService = $dealService;
        $this->batchService = $batchService;
        $this->crm = $crm;
    }

    public function easyShow($id)
    {
        $deal = $this->dealService->get(['ID' => $id]);
        return response()->json($deal, 200);
    }
}

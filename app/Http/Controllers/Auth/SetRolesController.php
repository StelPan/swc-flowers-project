<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Services\Database\RoleService;
use Illuminate\Http\Request;

class SetRolesController extends Controller
{
    private RoleService $service;

    public function __construct(RoleService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        $roles = $this->service->all();
        return view('auth.roles', compact('roles'));
    }

    public function set(Request $request)
    {
        $request->validate([
            'roles' => 'array|required'
        ]);

        $roles = $request->get('roles');

        $user = auth()->user();

        foreach ($roles as $role) {
            $user->assignRole($role);
        }

        return redirect()->to('/');
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\Bitrix24\BatchService;
use App\Services\Bitrix24\DealService;
use App\Services\Bitrix24\TaskService;
use App\Types\Task\Status;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Symfony\Component\HttpClient\Exception\ClientException;

class TaskController extends Controller
{
    private TaskService $taskService;

    private DealService $dealService;

    private BatchService $batchService;

    public function __construct(
        TaskService  $taskService,
        DealService  $dealService,
        BatchService $batchService
    )
    {
        $this->taskService = $taskService;
        $this->dealService = $dealService;
        $this->batchService = $batchService;
    }

    public function index(): \Illuminate\Http\JsonResponse
    {
        $tasks = $this->taskService->list([
            'RESPONSIBLE_ID' => auth('api')->user()->bitrix_id,
            '!STATUS' => Status::$STATE_COMPLETED
        ])['result']['tasks'];

        return response()->json([
            'error' => false,
            'data' => $tasks
        ]);
    }

    public function get(int $id): \Illuminate\Http\JsonResponse
    {
        $deal = $this->dealService->getDealByTaskId($id);
        return response()->json($deal, 200);
    }

    public function start(int $id)
    {
        // TODO: Проверять, принадлежит задача текущему флористу
        try {
            $result = $this->taskService->toStart(['taskId' => $id]);
            return response()->json($result, 200);
        } catch (ClientException $exception) {
            return response()->json(['message' => $exception->getResponse()->getContent(true)], 400);
        } catch (\Exception $exception) {
            return response()->json(['message' => $exception->getMessage()], 400);
        }
    }

    public function complete(int $id)
    {
        try {
            $result = $this->taskService->toComplete(['taskId' => $id]);
            return response()->json($result, 200);
        }  catch (ClientException $exception) {
            return response()->json(['message' => $exception->getResponse()->getContent(true)], 400);
        } catch (\Exception $exception) {
            return response()->json(['message' => $exception->getMessage()], 400);
        }
    }
}

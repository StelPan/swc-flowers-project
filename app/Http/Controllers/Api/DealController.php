<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\DealActionsTrait;
use App\Http\Requests\deal\UpdateDocketRequest;
use App\Http\Requests\deal\UpdateTrolAndSheflRequest;
use App\Services\Bitrix24\BatchService;
use App\Services\Bitrix24\DealService;
use Bitrix24\SDK\Services\CRM\Deal\Service\Deal;
use Illuminate\Http\Request;
use Symfony\Component\HttpClient\Exception\ClientException;

class DealController extends Controller
{
    use DealActionsTrait;

    private DealService $dealService;

    private BatchService $batchService;

    public function __construct(DealService $dealService, BatchService $batchService)
    {
        $this->dealService = $dealService;

        $this->batchService = $batchService;
    }
}

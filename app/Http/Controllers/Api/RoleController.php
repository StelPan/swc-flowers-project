<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\Database\RoleService;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    private RoleService $roleService;

    public function __construct(RoleService $roleService)
    {
        $this->roleService = $roleService;
    }

    public function all(): \Illuminate\Http\JsonResponse
    {
        $roles = $this->roleService->all();
        return response()->json($roles, 200);
    }
}

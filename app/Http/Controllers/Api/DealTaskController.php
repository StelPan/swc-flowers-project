<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\Bitrix24\DealService;
use App\Types\Deals\Stage;
use Illuminate\Http\Request;

class DealTaskController extends Controller
{
    private DealService $dealService;

    public function __construct(DealService $dealService)
    {
        $this->dealService = $dealService;
    }

    public function assemblyFlavorTask(int $id)
    {
        try {
            $deal = $this->dealService->store(['id' => $id]);
            if (!$deal) {
                return response()->json(['message' => 'Сделка не найдена.', 404]);
            }

            if ($deal['STAGE_ID'] !== Stage::$NEW) {
                return response()->json($deal, 200);
            }

            $create = $this->dealService->store([[
                'fields' => [
                    'TITLE' => 'Задача на сборку букета по сделке №' . $id,
                    'RESPONSIBLE_ID' => auth()->user()->bitrix_id,
                    'UF_CRM_TASK' => [
                        'D_' . $id
                    ]
                ]
            ]]);

            return response()->json(['action' => 'deals.tasks.create', $create], 200);
        } catch (\Exception $exception) {
            return response()->json($exception->getMessage(), $exception->getCode());
        }
    }
}

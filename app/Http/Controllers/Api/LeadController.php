<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\lead\CreateLeadRequest;
use App\Repositories\Database\ContactRepository;
use App\Services\Bitrix24\LeadService;
use App\Services\Database\ContactCustomerService;
use Bitrix24\SDK\Services\CRM\Contact\Service\Contact;
use GuzzleHttp\Exception\ClientException;

class LeadController extends Controller
{
    private LeadService $service;

    private ContactCustomerService $contactCustomerService;

    public function __construct(
        LeadService $service,
        ContactCustomerService $contactCustomerService,
    )
    {
        $this->service = $service;

        $this->contactCustomerService = $contactCustomerService;
    }

    public function add(CreateLeadRequest $request): \Illuminate\Http\JsonResponse
    {
        try {
            $result = $this->service->add($request->all());
            return response()->json($result, 201);
        } catch (\Exception $exception) {
            return response()->json(['message' => $exception->getMessage()], $exception->getCode());
        }
    }
}

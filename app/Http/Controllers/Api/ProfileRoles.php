<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Role;
use Illuminate\Http\Request;

class ProfileRoles extends Controller
{
    public function setProfileRoles(Request $request): \Illuminate\Http\JsonResponse
    {
        $validated = $request->validate([
            'roles' => 'required|array',
            'roles.*' => 'exists:roles,id'
        ]);

        $roles = $validated['roles'];

        $user = auth('api')->user();
        $user->assignRole($roles);

        return response()->json(['error' => false, 'message' => 'Роли установлены'], 200);
    }
}

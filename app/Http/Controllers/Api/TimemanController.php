<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\TimemanActionsTrait;
use App\Services\Bitrix24\TimemanService;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TimemanController extends Controller
{
    use TimemanActionsTrait;

    private TimemanService $service;

    public function __construct(TimemanService $service)
    {
        $this->service = $service;
    }
}

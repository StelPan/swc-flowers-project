<?php

namespace App\Http\Controllers\Roles\Logistician;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LogisticianHomeController extends Controller
{
    public function __construct()
    {
        //
    }

    public function home()
    {
        $camera_index = auth()->user()->camera_index ?? 0;
        return view('roles.logistician.home', compact('camera_index'));
    }
}

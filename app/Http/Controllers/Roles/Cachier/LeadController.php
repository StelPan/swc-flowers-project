<?php

namespace App\Http\Controllers\Roles\Cachier;

use App\Http\Controllers\Controller;
use App\Http\Requests\lead\CreateLeadRequest;
use App\Services\Bitrix24\LeadService;
use App\Services\Database\ContactCustomerService;

class LeadController extends Controller
{
    /**
     * @var ContactCustomerService
     */
    private ContactCustomerService $contactCustomerService;

    /**
     * @var LeadService
     */
    private LeadService $leadService;

    public function __construct(
        ContactCustomerService $contactCustomerService,
        LeadService $leadService,
    )
    {
        $this->contactCustomerService = $contactCustomerService;

        $this->leadService = $leadService;
    }

    public function create()
    {
        return view('roles.cachier.contacts.index');
    }

    public function store(CreateLeadRequest $request): \Illuminate\Http\RedirectResponse
    {
        try {
            $leadId = $this->leadService->add($request->all());
            return redirect()->back()->with('success', 'Лид был успешно создан под номером ' . $leadId);
        } catch (\Exception $exception) {
            return redirect()->back()->withErrors(['error' => $exception->getMessage()]);
        }
    }
}

<?php

namespace App\Http\Controllers\Roles\Administrator;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\User;
use App\Repositories\Database\RoleRepository;
use App\Services\Bitrix24\BatchService;
use App\Services\Bitrix24\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Spatie\FlareClient\Http\Exceptions\NotFound;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserController extends Controller
{
    private RoleRepository $roleRepository;

    private UserService $userService;

    private BatchService $batchService;

    public function __construct(RoleRepository $roleRepository,UserService $userService, BatchService $batchService)
    {
        $this->roleRepository = $roleRepository;

        $this->userService = $userService;

        $this->batchService = $batchService;
    }

    public function index()
    {
        try {
            $users = User::with('roles')->get();
            $bitrixUsers = $this->userService->get();

            $commands = $this->commands(ceil($bitrixUsers['total'] / 50));

            $bitrixUsers = $this->batchService->batch($commands);
            $bitrixUsers = collect($bitrixUsers['result'])->flatMap(fn($user) => $user);

            return view('roles.administrator.users.index', compact('users', 'bitrixUsers'));
        } catch (\Exception $exception) {
            dd($exception);
        }
    }

    public function edit(Request $request, int $user)
    {
        $user = User::with('roles')->findOrFail($user);
        $roles = Role::all();

        $externalUser = $user->bitrix_id ?
            $this->userService->get(['id' => $user->bitrix_id]) :
            null;

        if (!$externalUser) {
            throw new NotFound('Пользователя в Битрикс24 отсутствует.');
        }

        return view('roles.administrator.users.edit', compact('user', 'roles'));
    }

    public function update(Request $request, int $user)
    {
        $user = User::with('roles')->findOrFail($user);

        $externalUser = $user->bitrix_id ?
            $this->userService->get(['id' => $user->bitrix_id])['result'] :
            null;

        if (!$externalUser) {
            throw new NotFound('Пользователь в Битрикс24 отсутствует.');
        }

        $externalUser = $externalUser[0];

        $roles = $request->get('roles');

        foreach ($user->roles as $role) {
            $user->removeRole($role);
        }

        foreach ($roles as $role) {
            $user->assignRole($role);
        }

        $isGenerate = (bool)$request->get('generate');
        $password = $isGenerate ? Str::random(10) : null;

        if ($isGenerate) {
            $user->password = Hash::make($password);
            $user->save();
        }

        $request->session()->flash('password', $password);

        return redirect()->route('users.edit', ['user' => $user])->with('success', 'Права успешно изменены.');
    }

    public function create(int $id)
    {
        $roles = $this->roleRepository->all();

        $externalUser = $this->userService->get(['id' => $id])[0];
        if (!$externalUser) {
            throw new NotFoundHttpException('Пользователь в Б24 с ID: ' . $id . 'не найден.');
        }

        return view('roles.administrator.users.create', compact('externalUser', 'roles'));
    }

    public function store(Request $request, int $user)
    {
        $response = $this->userService->get(['ID' => $user]);

        $external = $response ? $response[0] : null;
        if (!$external) {
            throw new \Exception('Пользователь в Б24 отсутствует');
        }

        $internal = User::where('bitrix_id', '=', $external['ID'])->first();
        if ($internal) {
            throw new \Exception('Пользователь уже существует.');
        }

        $roles = $request->get('roles');

        $password = Str::random(10);

        $create = User::create([
            'name' => $external['NAME'],
            'email' => $external['EMAIL'],
            'password' => Hash::make($password),
            'bitrix_id' => $external['ID']
        ]);

        foreach ($roles as $role) {
            $create->assignRole($role);
        }

        return redirect()
            ->route('users.index')
            ->with(
                'admin.user.created',
                'Пользователь успешно создан.' . "\r\n" . 'Логин: ' . $create['email'] . "\r\n" . 'Пароль: ' . $password
            );
    }

    public function commands($pages): array
    {
        $batch = [];

        for ($i = 1; $i <= $pages; $i++) {
            $batch['get_' . $i] = 'user.get?' . http_build_query([
                    'start' => '$next[get_' . ($i - 1) . ']',
                ]);
        }

        return $batch;
    }
}

<?php

namespace App\Http\Controllers\Roles\Grower;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PhotoController extends Controller
{
    public function view($id, Request $request)
    {
        $url = $request->get('photo_url');
        return view('roles.grower.photo-view', compact('url', 'id'));
    }
}

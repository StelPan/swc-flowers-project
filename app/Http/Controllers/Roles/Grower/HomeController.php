<?php

namespace App\Http\Controllers\Roles\Grower;

use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index()
    {
        return view('roles.grower.home');
    }

    public function chat()
    {
        return view('roles.grower.chat');
    }
}

<?php

namespace App\Http\Controllers\Roles\Grower;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ChatController extends Controller
{
    public function index()
    {
        $wic = config('wazzup.wazzup_iframe_src');
        return view('roles.grower.chat', compact('wic'));
    }
}

<?php

namespace App\Http\Controllers\Roles\Grower;

use App\Http\Controllers\Controller;
use App\Services\Bitrix24\DealService;
use App\Services\Bitrix24\TaskService;
use App\Types\Task\Status;

class TaskController extends Controller
{
    private TaskService $taskService;

    private DealService $dealService;

    public function __construct(
        TaskService  $taskService,
        DealService  $dealService,
    )
    {
        $this->taskService = $taskService;
        $this->dealService = $dealService;
    }

    public function index()
    {
        $tasks = $this->taskService->list([
            'filter' => [
                'RESPONSIBLE_ID' => auth()->user()->bitrix_id,
                '!STATUS' => Status::$STATE_COMPLETED,
            ],
            'order' => ['ID' => 'desc']
        ])['result']['tasks'];

        return view('roles.grower.tasks', compact('tasks'));
    }

    public function edit(int $id)
    {
       try {
           $deal = $this->dealService->getDealByTaskId($id);
           $deal = array_merge($deal, ['BASE_URL' => config('bitrix.base_url')]);

           return view('roles.grower.task-edit', compact('deal'));
       } catch (\Exception $exception) {
           return redirect()->back()->withErrors([$exception->getMessage()]);
       }
    }

    public function start(int $id): \Illuminate\Http\RedirectResponse
    {
        // TODO: Проверять, принадлежит задача текущему флористу
        try {
            $this->taskService->toStart(['taskId' => $id]);

            return redirect()->route('growers.tasks.edit', ['id' => $id])
                ->with('grower.status.edit', 'Статус задачи изменен на "Выполняется".');
        } catch (\Exception $exception) {
            return redirect()
                ->back()
                ->withErrors(["error" => $exception->getMessage()]);
        }
    }

    public function complete(int $id): \Illuminate\Http\RedirectResponse
    {
        try {
            $this->taskService->toComplete(['taskId' => $id]);
            return redirect()->route('growers.tasks')
                ->with('tasks.task.completed', 'Вы завершили задачу.');
        } catch (\Exception $exception) {
            return redirect()
                ->back()
                ->withErrors(["error" => $exception->getMessage()]);
        }
    }
}

<?php

namespace App\Http\Controllers\Roles\Packer;

use App\Http\Controllers\Controller;
use App\Services\Bitrix24\BatchService;
use App\Services\Bitrix24\DealService;
use Bitrix24\SDK\Core\ApiClient;
use GuzzleHttp\Exception\ClientException;

class HomeController extends Controller
{
    private DealService $dealService;

    private BatchService $batchService;

    public function __construct(DealService $service, BatchService $batchService)
    {
        $this->dealService = $service;

        $this->batchService = $batchService;
    }

    public function search()
    {
        return view ('roles.packer.search');
    }

    public function storage(int $id)
    {
        try {
            $deal = $this->dealService->get(['ID' => $id]);
            $deal = array_merge($deal, ['USER_FIELDS' => config('bitrix.user_fields.deals')]);
            return view('roles.packer.storage', compact('deal'));
        } catch (\Exception $exception) {
            return redirect()->to('/packer/search')->withErrors(['Сделка не найдена']);
        }
    }

    public function dealRequest(int $id)
    {
        $deal = $this->dealService->get(['ID' => $id]);
        if (empty($deal)) {
            return response()->json(['message' => 'Сделка не найдена в Б24'], 404);
        }

        $batch = array_merge($deal, $this->batchService->batch([
            'ASSIGNED' => 'user.get?' . http_build_query(['id' => $deal['ASSIGNED_BY_ID']]),
            'PRODUCTS' => 'crm.deal.productrows.get?' . http_build_query(['id' => $id]),
            'TASKS' => 'tasks.task.list?' . http_build_query(['filter' => ['UF_CRM_TASK' => 'D_' . $id]]),
        ])['result']);

        $deal['CUSTOM_FIELDS'] = config('bitrix.user_fields.deals');

        return $deal;
    }
}

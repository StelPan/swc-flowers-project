<?php

namespace App\Http\Controllers\Roles\Packer;

use App\Http\Controllers\Controller;
use App\Services\Bitrix24\BatchService;
use App\Services\Bitrix24\DealService;
use Illuminate\Http\Request;

class DealController extends Controller
{
    private DealService $dealService;

    private BatchService $batchService;

    public function __construct(DealService $dealService, BatchService $batchService)
    {
        $this->dealService = $dealService;

        $this->batchService = $batchService;
    }

    public function storage (int $id)
    {

    }
}

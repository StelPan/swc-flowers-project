<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

class RedirectRoleController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function redirectRoleRoute(): \Illuminate\Http\RedirectResponse
    {
        return redirect('/' . $this->getRole()->name);
    }

    public function getRole()
    {
        return Auth::user()->roles()->first();
    }
}

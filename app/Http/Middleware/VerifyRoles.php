<?php

namespace App\Http\Middleware;

use App\Services\Database\UserService;
use Closure;
use Illuminate\Http\Request;

class VerifyRoles
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $roles = auth()->user()->roles()->get();
        if (!$roles->count()) {
            return redirect()->route('auth.roles.view');
        }

        return $next($request);
    }
}

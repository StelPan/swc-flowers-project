<?php

namespace App\Http\Middleware;

use App\Bitrix24\Bitrix24API;
use Bitrix24\SDK\Core\ApiClient;
use Closure;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BitrixAuthenticate
{
    private ApiClient $client;

    public function __construct(ApiClient $client)
    {
        $this->client = $client;
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse) $next
     * @return \Illuminate\Http\JsonResponse
     * @throws AuthenticationException
     */
    public function handle(Request $request, Closure $next)
    {
        $user = Auth::user();
        if (!$user) {
            throw new AuthenticationException('Пользователь не определен.');
        }

        if (!$user->bitrix_id) {
            return response()->json(['message' => 'Пользователь не синхронизирован с Б24'], 404);
        }

        return $next($request);
    }
}

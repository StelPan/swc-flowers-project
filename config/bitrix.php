<?php

return [
    # Базовы УРЛ Битрикс24 без СЛЭША на конце
    'base_url' => env('BITRIX_BASE_URL', ''),

    # Входящий веб-хук
    # Обязательные права на веб-хук для работы приложения:
    # task, user, crm, catalog, tasks, timeman, im.
    'webhook' => env('BITRIX_WEBHOOK', ''),

    # Секретный код
    # Код предназначен для закрепления задачи за специалистом florcat
    'secret_code' => env('BITRIX_SECRET_CODE', ''),

    # Идентификатор пользователя от которого будет отправляться логины и пароли
    # для доступа в систему
    'message_sender_id' => '',

    # События, происходящие на стороне БИТРИКС24
    'events' => [
        # Добавление контакта
        'ONCRMCONTACTADD' => \App\Events\Bitrix24\ContactCreated::class,
        # Обновление контакта
        'ONCRMCONTACTUPDATE' => \App\Events\Bitrix24\ContactUpdated::class,
        # Создание сотрудника
        'ONUSERADD' => \App\Events\Bitrix24\UserCreated::class,
    ],

    # Пользовательские поля
    'user_fields' => [
        # Поля сделки
        'deals' => [
            # Файл с QR - кодом сделки
            'QR' => 'UF_CRM_1679392519932', #  UF_CRM_1679392519932
            # Комментарий флориста
            'COMMENT_FOR_GROWER' => 'UF_CRM_1689663267798',
            # Адрес доставки
            'DELIVERY_ADDRESS' => 'UF_CRM_5D986DC49E8D5',
            # Дата и время начал доставки
            'DELIVERY_START_DATE' => 'UF_CRM_1689663335261',
            # Дата и время окончания доставки
            'DELIVERY_END_DATE' => 'UF_CRM_1689663357957',
            # Состав букета
            'COMPOSITION' => 'UF_CRM_1689185741447',
            # Этикетка
            'DOCKET' => 'UF_CRM_1689185765026',
            # Троль и полка
            'TROL_AND_SHEFL' => 'UF_CRM_1693387089435', # external: UF_CRM_1693387089435, internal: UF_CRM_1694369197572
            # Товары
            "GOODS_DESCRIPTION" => 'UF_CRM_1689663432389',
            # курьер
            "COURIER" => 'UF_CRM_1667981774966',
        ],
        'leads' => [
            # Количество списываемых бонусов
            'BONUS' => 'UF_CRM_1693387566815', # external: UF_CRM_1693387566815, internal: UF_CRM_1694380695338
        ]
    ],
];

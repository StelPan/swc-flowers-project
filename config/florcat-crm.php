<?php

return [
    'base_url' => env('BITRIX_BASE_URL', ''),

    'secret_code' => env('BITRIX_SECRET_CODE', ''),
];

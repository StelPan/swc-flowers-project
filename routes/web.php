<?php

use App\Http\Controllers\BitrixWebhookController;
use App\Http\Controllers\Roles\Cachier\ContactController;
use App\Http\Controllers\Roles\Cachier\LeadController;
use Illuminate\Support\Facades\Route;

Auth::routes([
    'confirm' => false, 'forgot' => false, 'login' => true,
    'register' => false, 'reset' => false, 'verification' => false,
]);

Route::get('/', function () {
    return redirect()->route('login');
});

Route::get('logs', [\Rap2hpoutre\LaravelLogViewer\LogViewerController::class, 'index']);

# Все вебхуки поступающие от Б24
Route::post('/bitrix/webhooks', [BitrixWebhookController::class, 'emit']);

# Маршруты для установки ролей
Route::group(['middleware' => ['auth', 'auth.bitrix']], function () {
    Route::get('/auth/roles/set', [\App\Http\Controllers\Auth\SetRolesController::class, 'index'])->name('auth.roles.view');
    Route::post('/auth/roles/set', [\App\Http\Controllers\Auth\SetRolesController::class, 'set'])->name('auth.roles.set');
});

Route::group(['middleware' => ['auth', 'auth.bitrix', 'verify_roles']], function () {
    Route::get('/home', [\App\Http\Controllers\RedirectRoleController::class, 'redirectRoleRoute']);

    # Маршруты для пользователя с ролью кассир
    Route::group(['middleware' => ['role:cachier'], 'prefix' => 'cachier'], function () {
        Route::get('/', function () {
            return redirect()->route('leads.create');
        });

        Route::get('/leads/create', [LeadController::class, 'create'])->name('leads.create');
        Route::post('/leads/store', [LeadController::class, 'store'])->name('leads.store');
    });

    # Маршруты для пользователя с ролью флорист
    Route::group(['middleware' => ['role:grower'], 'prefix' => 'grower'], function () {
        Route::get('/', [\App\Http\Controllers\Roles\Grower\HomeController::class, 'index'])
            ->name('growers.index');

        Route::get('/chat', [\App\Http\Controllers\Roles\Grower\ChatController::class, 'index'])
            ->name('growers.chat');

        Route::get('/tasks', [\App\Http\Controllers\Roles\Grower\TaskController::class, 'index'])
            ->name('growers.tasks');

        Route::get('/tasks/{id}/edit', [\App\Http\Controllers\Roles\Grower\TaskController::class, 'edit'])
            ->name('growers.tasks.edit');

        Route::post('/tasks/{id}/start', [\App\Http\Controllers\Roles\Grower\TaskController::class, 'start'])
            ->name('growers.tasks.start');

        Route::post('/tasks/{id}/complete', [\App\Http\Controllers\Roles\Grower\TaskController::class, 'complete'])
            ->name('growers.tasks.complete');

        Route::get('/tasks/{id}/photos/view', [\App\Http\Controllers\Roles\Grower\PhotoController::class, 'view']);
    });

    # Маршруты для пользователя с ролью упаковщик
    Route::group(['middleware' => ['role:packer'], 'prefix' => 'packer'], function () {
        Route::get('/', fn() => redirect('/packer/search'));

        Route::get('/search', [\App\Http\Controllers\Roles\Packer\HomeController::class, 'search'])
            ->name('packer.search-deal');

        Route::get('/deals/{id}/storage', [\App\Http\Controllers\Roles\Packer\HomeController::class, 'storage'])
            ->name('packer.storage-deal');
    });

    # Маршруты для логиста
    Route::group(['middleware' => ['role:logistician'], 'prefix' => 'logistician'], function () {
        Route::get('/', [\App\Http\Controllers\Roles\Logistician\LogisticianHomeController::class, 'home']);
    });

    # Маршруты для администратора
    Route::group(['middleware' => ['role:administrator'], 'prefix' => 'administrator'], function () {
        Route::get('/', static fn() => redirect('/administrator/users'));

        Route::resource('users', \App\Http\Controllers\Roles\Administrator\UserController::class)
            ->only('index', 'edit', 'update');

        Route::get('/users/{user}/create', [\App\Http\Controllers\Roles\Administrator\UserController::class, 'create'])
            ->name('users.create');

        Route::post('/users/{user}/store', [\App\Http\Controllers\Roles\Administrator\UserController::class, 'store'])
            ->name('users.store');
    });
});

Route::get('/test', function () {
    $contact = new \App\Models\Contact();
    dd($contact->bitrix_contact_id);
});

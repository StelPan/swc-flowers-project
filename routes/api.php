<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\TaskController;
use App\Http\Controllers\Api\TimemanController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

# Маршруты авторизации пользователя

Route::group(['prefix' => 'auth'], function () {
    Route::post('/login', [AuthController::class, 'login']);
    Route::post('/logout', [AuthController::class, 'logout'])->middleware('auth:api');
    Route::post('/refresh', [AuthController::class, 'refresh'])->middleware('auth:api');
    Route::post('/me', [AuthController::class, 'me'])->middleware('auth:api');
    Route::post('/set-roles', [\App\Http\Controllers\Api\ProfileRoles::class, 'setProfileRoles'])->middleware(
        'auth:api'
    );
});


# Маршруты для кассира
Route::group(['prefix' => 'cachier', 'middleware' => ['auth:api', 'auth.bitrix']], function () {

    # Маршруты контактов
    Route::group(['prefix' => 'contacts', 'middleware' => ['auth:api', 'auth.bitrix']], function () {
        Route::get('/search', [\App\Http\Controllers\Api\ContactController::class, 'get']);
    });

    # Маршруты лидов
    Route::group(['prefix' => 'leads'], function () {
        Route::post('/', [\App\Http\Controllers\Api\LeadController::class, 'add']);
    });
});

# Маршруты ролей пользователей
Route::group(['prefix' => 'roles', 'middleware' => ['auth:api', 'auth.bitrix']], function () {
    Route::get('/', [\App\Http\Controllers\Api\RoleController::class, 'all']);
});

# Маршруты для работы со сделками
Route::group(['prefix' => 'deals', 'name' => 'deals.', 'middleware' => ['auth:api', 'auth.bitrix']], function () {
    Route::get('/{id}', [\App\Http\Controllers\Api\DealController::class, 'get'])->name('get');
    Route::post('/{id}/storage', [\App\Http\Controllers\Api\DealController::class, 'storage']);
});

# Маршруты рабочего времени

Route::group(['prefix' => 'timeman', 'middleware' => ['auth:api', 'auth.bitrix', 'role:grower']], function () {
    Route::get('/open', [TimemanController::class, 'open'])->name('timeman.open');
    Route::get('/pause', [TimemanController::class, 'pause'])->name('timeman.pause');
    Route::get('/close', [TimemanController::class, 'close'])->name('timeman.close');
    Route::get('/status', [TimemanController::class, 'status'])->name('timeman.status');
});

# Маршруты для флориста

Route::group(['prefix' => 'grower', 'middleware' => ['auth:api', 'auth.bitrix', 'role:grower']], function () {
    Route::group(['prefix' => 'tasks'], function () {
        Route::get('/', [TaskController::class, 'index']);
        Route::get('/{id}', [TaskController::class, 'get']);
        Route::post('/{id}/start', [TaskController::class, 'start']);
        Route::post('/{id}/complete', [TaskController::class, 'complete']);
    });
});

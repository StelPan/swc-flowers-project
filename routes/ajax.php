<?php

use App\Http\Controllers\Ajax\ContactController;
use App\Http\Controllers\Ajax\DealController;
use App\Http\Controllers\Ajax\DealTaskController;
use App\Http\Controllers\Ajax\ProfileController;
use App\Http\Controllers\Ajax\TaskController;
use App\Http\Controllers\Ajax\TimemanController;
use Illuminate\Support\Facades\Route;

# Маршруты для обращения по Ajax

Route::middleware(['auth', 'auth.bitrix'])->group(function () {
    Route::group(['prefix' => 'deals', 'middleware' => ['role:grower', 'role:cachier', 'role:packer']], function () {
        Route::get('/{id}', [DealController::class, 'get']);
        Route::get('/{id}/easy', [DealController::class, 'easyShow']);
        Route::post('/{id}/docket', [DealController::class, 'docket']);
        Route::post('/{id}/storage', [DealController::class, 'storage']);
        Route::post('/{id}/tasks', [DealTaskController::class, 'assemblyFlavorTask']);
    });

    Route::group(['prefix' => 'tasks', 'middleware' => ['role:grower']], function () {
        Route::get('/{id}', [TaskController::class, 'edit']);
        Route::post('/{id}/set-by-qr', [TaskController::class, 'setByQr']);
        Route::post('/{id}/start', [TaskController::class, 'start']);
        Route::post('/{id}/complete', [TaskController::class, 'complete']);
    });

    Route::group(['prefix' => 'contacts', 'middleware' => ['role:cachier']], function () {
        Route::get('/', [ContactController::class, 'get']);
    });

    Route::group(['prefix' => 'profile'], function () {
        Route::get('/update-camera', [ProfileController::class, 'updateCameraIndex']);
    });

    Route::group(['prefix' => 'timeman', 'middleware' => ['role:grower']], function () {
        Route::get('/open', [TimemanController::class, 'open'])->name('timeman.open');
        Route::get('/pause', [TimemanController::class, 'pause'])->name('timeman.pause');
        Route::get('/close', [TimemanController::class, 'close'])->name('timeman.close');
        Route::get('/status', [TimemanController::class, 'status'])->name('timeman.status');
    });
});

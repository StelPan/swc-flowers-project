<?php

return [
    [
        'name' => \App\Types\RoleTypes::$CACHIER,
        'ru_name' => 'Кассир'
    ],
    [
        'name' => \App\Types\RoleTypes::$GROWER,
        'ru_name' => 'Флорист'
    ],
    [
        'name' => \App\Types\RoleTypes::$COURIER,
        'ru_name' => 'Курьер'
    ],
    [
        'name' => \App\Types\RoleTypes::$PACKER,
        'ru_name' => 'Упаковщик'
    ],
    [
        'name' => \App\Types\RoleTypes::$LOGISTICIAN,
        'ru_name' => 'Логист'
    ],
    [
        'name' => \App\Types\RoleTypes::$ADMINISTRATOR,
        'ru_name' => 'Администратор'
    ],
];
